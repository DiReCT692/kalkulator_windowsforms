﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalk_WindowsForms
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (UserInputText.Text == "0")
            {
                UserInputText.Clear();
            }
            Button button = (Button)sender;
            if (button.Text == ",")
            {
                if (!UserInputText.Text.Contains(","))
                    UserInputText.Text = UserInputText.Text + button.Text;
            }
            else
            {
                UserInputText.Text = UserInputText.Text + button.Text;
            }
        }

        private void buttonBackspace_Click(object sender, EventArgs e)
        {
            if (UserInputText.Text.Length > 0)
            {
                UserInputText.Text = UserInputText.Text.Remove(UserInputText.Text.Length - 1, 1);
                if (UserInputText.Text.Length == 0)
                {
                    UserInputText.Text = "0";
                }
            }
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            UserInputText.Clear();
            UserInputText.Text = "0";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double health = 0.09;
            double disabilityPension = 0.015;
            double pension = 0.0976;
            double sickness = 0.0245;
            Console.WriteLine("Set gross salary basis");
            double salary = double.Parse(UserInputText.Text);
            double social = (salary * pension) + (salary * sickness) + (salary * disabilityPension);
            double healthContribution = (salary - social) * health;
            Console.WriteLine("Net salary for you: {0}", salary - (social + healthContribution));
            Console.WriteLine("Sum of contribution: {0}", social + healthContribution);
            labelContribution.Text = ("Salary:\n" + salary + "\n\nPension:\n" + salary * health + "\n\nDisability Pension:\n" + salary * disabilityPension +
            "\n\nStickness:\n" + salary * sickness + "\n\nHealth:\n" + (salary - social) * health + "\n\nSum of social:\n" + social + "\n\nSum of Contributions:\n" + (social + healthContribution) + "\n\nNet salary:\n" + (salary - (social + healthContribution))).ToString();

        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
