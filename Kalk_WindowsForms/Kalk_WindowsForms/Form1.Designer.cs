﻿namespace Kalk_WindowsForms
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buttonLog = new System.Windows.Forms.Button();
            this.buttonPlusMinus = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.buttonEqual = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.buttonPower2 = new System.Windows.Forms.Button();
            this.buttonSub = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.buttonSquare = new System.Windows.Forms.Button();
            this.buttonMul = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.buttonPowerx = new System.Windows.Forms.Button();
            this.buttonDiv = new System.Windows.Forms.Button();
            this.buttonCtg = new System.Windows.Forms.Button();
            this.buttonTg = new System.Windows.Forms.Button();
            this.buttonCos = new System.Windows.Forms.Button();
            this.buttonSin = new System.Windows.Forms.Button();
            this.buttonNettoBrutto = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.buttonZusEmployer = new System.Windows.Forms.Button();
            this.buttonPI = new System.Windows.Forms.Button();
            this.buttonBackspace = new System.Windows.Forms.Button();
            this.buttonDel = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.UserInputText = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // buttonLog
            // 
            this.buttonLog.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonLog.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonLog.Location = new System.Drawing.Point(13, 348);
            this.buttonLog.Name = "buttonLog";
            this.buttonLog.Size = new System.Drawing.Size(50, 50);
            this.buttonLog.TabIndex = 0;
            this.buttonLog.Text = "log";
            this.buttonLog.UseVisualStyleBackColor = true;
            this.buttonLog.Click += new System.EventHandler(this.buttonLog_Click);
            // 
            // buttonPlusMinus
            // 
            this.buttonPlusMinus.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonPlusMinus.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonPlusMinus.Location = new System.Drawing.Point(69, 348);
            this.buttonPlusMinus.Name = "buttonPlusMinus";
            this.buttonPlusMinus.Size = new System.Drawing.Size(50, 50);
            this.buttonPlusMinus.TabIndex = 1;
            this.buttonPlusMinus.Text = "+/-";
            this.buttonPlusMinus.UseVisualStyleBackColor = true;
            this.buttonPlusMinus.Click += new System.EventHandler(this.buttonPlusMinus_Click);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(125, 348);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(50, 50);
            this.button3.TabIndex = 2;
            this.button3.Text = "0";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.button4.Location = new System.Drawing.Point(181, 348);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 50);
            this.button4.TabIndex = 3;
            this.button4.Text = ",";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonEqual
            // 
            this.buttonEqual.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonEqual.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonEqual.Location = new System.Drawing.Point(237, 348);
            this.buttonEqual.Name = "buttonEqual";
            this.buttonEqual.Size = new System.Drawing.Size(50, 50);
            this.buttonEqual.TabIndex = 4;
            this.buttonEqual.Text = "=";
            this.buttonEqual.UseVisualStyleBackColor = true;
            this.buttonEqual.Click += new System.EventHandler(this.Equal_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonAdd.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonAdd.Location = new System.Drawing.Point(237, 292);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(50, 50);
            this.buttonAdd.TabIndex = 9;
            this.buttonAdd.Text = "+";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.operator_Click);
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button7.Location = new System.Drawing.Point(181, 292);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(50, 50);
            this.button7.TabIndex = 8;
            this.button7.Text = "3";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button1_Click);
            // 
            // button8
            // 
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button8.Location = new System.Drawing.Point(125, 292);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(50, 50);
            this.button8.TabIndex = 7;
            this.button8.Text = "2";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button1_Click);
            // 
            // button9
            // 
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button9.Location = new System.Drawing.Point(69, 292);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(50, 50);
            this.button9.TabIndex = 6;
            this.button9.Text = "1";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonPower2
            // 
            this.buttonPower2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonPower2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonPower2.Location = new System.Drawing.Point(13, 292);
            this.buttonPower2.Name = "buttonPower2";
            this.buttonPower2.Size = new System.Drawing.Size(50, 50);
            this.buttonPower2.TabIndex = 5;
            this.buttonPower2.Text = "x^2";
            this.buttonPower2.UseVisualStyleBackColor = true;
            this.buttonPower2.Click += new System.EventHandler(this.buttonPower2_Click);
            // 
            // buttonSub
            // 
            this.buttonSub.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSub.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonSub.Location = new System.Drawing.Point(237, 236);
            this.buttonSub.Name = "buttonSub";
            this.buttonSub.Size = new System.Drawing.Size(50, 50);
            this.buttonSub.TabIndex = 14;
            this.buttonSub.Text = "-";
            this.buttonSub.UseVisualStyleBackColor = true;
            this.buttonSub.Click += new System.EventHandler(this.operator_Click);
            // 
            // button12
            // 
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button12.Location = new System.Drawing.Point(181, 236);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(50, 50);
            this.button12.TabIndex = 13;
            this.button12.Text = "6";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button1_Click);
            // 
            // button13
            // 
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button13.Location = new System.Drawing.Point(125, 236);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(50, 50);
            this.button13.TabIndex = 12;
            this.button13.Text = "5";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button1_Click);
            // 
            // button14
            // 
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button14.Location = new System.Drawing.Point(69, 236);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(50, 50);
            this.button14.TabIndex = 11;
            this.button14.Text = "4";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonSquare
            // 
            this.buttonSquare.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSquare.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonSquare.Location = new System.Drawing.Point(12, 182);
            this.buttonSquare.Name = "buttonSquare";
            this.buttonSquare.Size = new System.Drawing.Size(50, 50);
            this.buttonSquare.TabIndex = 10;
            this.buttonSquare.Text = "√x";
            this.buttonSquare.UseVisualStyleBackColor = true;
            this.buttonSquare.Click += new System.EventHandler(this.buttonSquarex_Click);
            // 
            // buttonMul
            // 
            this.buttonMul.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonMul.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonMul.Location = new System.Drawing.Point(237, 180);
            this.buttonMul.Name = "buttonMul";
            this.buttonMul.Size = new System.Drawing.Size(50, 50);
            this.buttonMul.TabIndex = 19;
            this.buttonMul.Text = "*";
            this.buttonMul.UseVisualStyleBackColor = true;
            this.buttonMul.Click += new System.EventHandler(this.operator_Click);
            // 
            // button17
            // 
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button17.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button17.Location = new System.Drawing.Point(181, 180);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(50, 50);
            this.button17.TabIndex = 18;
            this.button17.Text = "9";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button1_Click);
            // 
            // button18
            // 
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button18.Location = new System.Drawing.Point(125, 180);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(50, 50);
            this.button18.TabIndex = 17;
            this.button18.Text = "8";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button1_Click);
            // 
            // button19
            // 
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button19.Location = new System.Drawing.Point(69, 180);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(50, 50);
            this.button19.TabIndex = 16;
            this.button19.Text = "7";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonPowerx
            // 
            this.buttonPowerx.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonPowerx.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonPowerx.Location = new System.Drawing.Point(13, 236);
            this.buttonPowerx.Name = "buttonPowerx";
            this.buttonPowerx.Size = new System.Drawing.Size(50, 50);
            this.buttonPowerx.TabIndex = 15;
            this.buttonPowerx.Text = "x^y";
            this.buttonPowerx.UseVisualStyleBackColor = true;
            this.buttonPowerx.Click += new System.EventHandler(this.buttonPowerx_Click);
            // 
            // buttonDiv
            // 
            this.buttonDiv.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDiv.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonDiv.Location = new System.Drawing.Point(237, 124);
            this.buttonDiv.Name = "buttonDiv";
            this.buttonDiv.Size = new System.Drawing.Size(50, 50);
            this.buttonDiv.TabIndex = 24;
            this.buttonDiv.Text = "/";
            this.buttonDiv.UseVisualStyleBackColor = true;
            this.buttonDiv.Click += new System.EventHandler(this.operator_Click);
            // 
            // buttonCtg
            // 
            this.buttonCtg.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonCtg.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonCtg.Location = new System.Drawing.Point(181, 124);
            this.buttonCtg.Name = "buttonCtg";
            this.buttonCtg.Size = new System.Drawing.Size(50, 50);
            this.buttonCtg.TabIndex = 23;
            this.buttonCtg.Text = "ctg(x)";
            this.buttonCtg.UseVisualStyleBackColor = true;
            this.buttonCtg.Click += new System.EventHandler(this.buttonCtg_Click);
            // 
            // buttonTg
            // 
            this.buttonTg.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonTg.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonTg.Location = new System.Drawing.Point(125, 124);
            this.buttonTg.Name = "buttonTg";
            this.buttonTg.Size = new System.Drawing.Size(50, 50);
            this.buttonTg.TabIndex = 22;
            this.buttonTg.Text = "tg(x)";
            this.buttonTg.UseVisualStyleBackColor = true;
            this.buttonTg.Click += new System.EventHandler(this.buttonTg_Click);
            // 
            // buttonCos
            // 
            this.buttonCos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonCos.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCos.Location = new System.Drawing.Point(69, 124);
            this.buttonCos.Name = "buttonCos";
            this.buttonCos.Size = new System.Drawing.Size(50, 50);
            this.buttonCos.TabIndex = 21;
            this.buttonCos.Text = "cos(x)";
            this.buttonCos.UseVisualStyleBackColor = true;
            this.buttonCos.Click += new System.EventHandler(this.buttonCos_Click);
            // 
            // buttonSin
            // 
            this.buttonSin.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSin.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSin.Location = new System.Drawing.Point(13, 124);
            this.buttonSin.Name = "buttonSin";
            this.buttonSin.Size = new System.Drawing.Size(50, 50);
            this.buttonSin.TabIndex = 20;
            this.buttonSin.Text = "sin(x)";
            this.buttonSin.UseVisualStyleBackColor = true;
            this.buttonSin.Click += new System.EventHandler(this.buttonSin_Click);
            // 
            // buttonNettoBrutto
            // 
            this.buttonNettoBrutto.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonNettoBrutto.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonNettoBrutto.Location = new System.Drawing.Point(181, 68);
            this.buttonNettoBrutto.Name = "buttonNettoBrutto";
            this.buttonNettoBrutto.Size = new System.Drawing.Size(106, 50);
            this.buttonNettoBrutto.TabIndex = 28;
            this.buttonNettoBrutto.Text = "Netto/Brutto";
            this.buttonNettoBrutto.UseVisualStyleBackColor = true;
            this.buttonNettoBrutto.Click += new System.EventHandler(this.buttonNettoBrutto_Click);
            // 
            // button28
            // 
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button28.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.button28.Location = new System.Drawing.Point(125, 68);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(50, 50);
            this.button28.TabIndex = 27;
            this.button28.Text = "ZUS²";
            this.toolTip1.SetToolTip(this.button28, "Employer");
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.buttonZusEmployee_Click);
            // 
            // buttonZusEmployer
            // 
            this.buttonZusEmployer.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonZusEmployer.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonZusEmployer.Location = new System.Drawing.Point(69, 68);
            this.buttonZusEmployer.Name = "buttonZusEmployer";
            this.buttonZusEmployer.Size = new System.Drawing.Size(50, 50);
            this.buttonZusEmployer.TabIndex = 26;
            this.buttonZusEmployer.Text = "ZUS¹";
            this.toolTip1.SetToolTip(this.buttonZusEmployer, "Employee");
            this.buttonZusEmployer.UseVisualStyleBackColor = true;
            this.buttonZusEmployer.Click += new System.EventHandler(this.buttonZusEmployer_Click);
            // 
            // buttonPI
            // 
            this.buttonPI.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonPI.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonPI.Location = new System.Drawing.Point(13, 68);
            this.buttonPI.Name = "buttonPI";
            this.buttonPI.Size = new System.Drawing.Size(50, 50);
            this.buttonPI.TabIndex = 25;
            this.buttonPI.Text = "π";
            this.buttonPI.UseVisualStyleBackColor = true;
            this.buttonPI.Click += new System.EventHandler(this.buttonPI_Click);
            // 
            // buttonBackspace
            // 
            this.buttonBackspace.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonBackspace.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonBackspace.Location = new System.Drawing.Point(69, 12);
            this.buttonBackspace.Name = "buttonBackspace";
            this.buttonBackspace.Size = new System.Drawing.Size(50, 50);
            this.buttonBackspace.TabIndex = 31;
            this.buttonBackspace.Text = "⇽";
            this.buttonBackspace.UseVisualStyleBackColor = true;
            this.buttonBackspace.Click += new System.EventHandler(this.buttonBackspace_Click);
            // 
            // buttonDel
            // 
            this.buttonDel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonDel.Location = new System.Drawing.Point(13, 12);
            this.buttonDel.Name = "buttonDel";
            this.buttonDel.Size = new System.Drawing.Size(50, 50);
            this.buttonDel.TabIndex = 30;
            this.buttonDel.Text = "DEL";
            this.buttonDel.UseVisualStyleBackColor = true;
            this.buttonDel.Click += new System.EventHandler(this.buttonDel_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label.Location = new System.Drawing.Point(125, 12);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(0, 16);
            this.label.TabIndex = 32;
            // 
            // UserInputText
            // 
            this.UserInputText.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UserInputText.Location = new System.Drawing.Point(125, 28);
            this.UserInputText.Multiline = true;
            this.UserInputText.Name = "UserInputText";
            this.UserInputText.Size = new System.Drawing.Size(162, 34);
            this.UserInputText.TabIndex = 33;
            this.UserInputText.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 407);
            this.Controls.Add(this.UserInputText);
            this.Controls.Add(this.label);
            this.Controls.Add(this.buttonBackspace);
            this.Controls.Add(this.buttonDel);
            this.Controls.Add(this.buttonNettoBrutto);
            this.Controls.Add(this.button28);
            this.Controls.Add(this.buttonZusEmployer);
            this.Controls.Add(this.buttonPI);
            this.Controls.Add(this.buttonDiv);
            this.Controls.Add(this.buttonCtg);
            this.Controls.Add(this.buttonTg);
            this.Controls.Add(this.buttonCos);
            this.Controls.Add(this.buttonSin);
            this.Controls.Add(this.buttonMul);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.buttonPowerx);
            this.Controls.Add(this.buttonSub);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.buttonSquare);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.buttonPower2);
            this.Controls.Add(this.buttonEqual);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buttonPlusMinus);
            this.Controls.Add(this.buttonLog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Kalkulator naukowy - 24461";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLog;
        private System.Windows.Forms.Button buttonPlusMinus;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button buttonEqual;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button buttonPower2;
        private System.Windows.Forms.Button buttonSub;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button buttonSquare;
        private System.Windows.Forms.Button buttonMul;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button buttonPowerx;
        private System.Windows.Forms.Button buttonDiv;
        private System.Windows.Forms.Button buttonCtg;
        private System.Windows.Forms.Button buttonTg;
        private System.Windows.Forms.Button buttonCos;
        private System.Windows.Forms.Button buttonSin;
        private System.Windows.Forms.Button buttonNettoBrutto;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button buttonZusEmployer;
        private System.Windows.Forms.Button buttonPI;
        private System.Windows.Forms.Button buttonBackspace;
        private System.Windows.Forms.Button buttonDel;
        private System.Windows.Forms.Label label;
        public System.Windows.Forms.TextBox UserInputText;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

