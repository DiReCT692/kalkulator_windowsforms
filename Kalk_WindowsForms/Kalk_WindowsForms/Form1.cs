﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalk_WindowsForms
{
    public partial class Form1 : Form
    {
        double result = 0;
        double opResult = 0;
        string operationPerformed = "";
        bool isOperationPerformed = false;
        bool divideByZero = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (UserInputText.Text == "0" || isOperationPerformed)
            {
                UserInputText.Clear();
            }
            isOperationPerformed = false;
            Button button = (Button)sender;
            if (button.Text == ",")
            {
                if (!UserInputText.Text.Contains(","))
                    UserInputText.Text = UserInputText.Text + button.Text;
            }
            else
            {
                UserInputText.Text = UserInputText.Text + button.Text;
            }
        }
            

        private void operator_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (result != 0)
            {
                buttonEqual.PerformClick();
                operationPerformed = button.Text;
                label.Text = result + " " + operationPerformed;
                isOperationPerformed = true;
            }
            else
            {
                operationPerformed = button.Text;
                result = double.Parse(UserInputText.Text);
                label.Text = result + " " + operationPerformed;
                isOperationPerformed = true;
            }
        }

        private void Equal_Click(object sender, EventArgs e)
        {
            switch (operationPerformed)
            {
                case "+":
                    opResult = result;
                    UserInputText.Text = (result + double.Parse(UserInputText.Text)).ToString();
                    result = 0;
                    break;
                case "-":
                    opResult = result;
                    UserInputText.Text = (result - double.Parse(UserInputText.Text)).ToString();
                    result = 0;
                    break;
                case "*":
                    opResult = result;
                    UserInputText.Text = (result * double.Parse(UserInputText.Text)).ToString();
                    result = 0;
                    break;
                case "/":
                    if (double.Parse(UserInputText.Text) != 0)
                    {
                        opResult = result;
                        UserInputText.Text = (result / double.Parse(UserInputText.Text)).ToString();
                        result = 0;
                    } 
                    else
                    {
                        divideByZero = true;
                        UserInputText.Text = "0";
                        label.Text = "You can't divide by zero";
                    }
                    break;
                case "x^y":
                    double power = Math.Pow(result, double.Parse(UserInputText.Text));
                    UserInputText.Text = power.ToString();
                    break;
                default:
                    break;
            }
            if (divideByZero == false)
            {
                result = opResult;
                label.Text = "";
            }
        }

        private void buttonBackspace_Click(object sender, EventArgs e)
        {
            if (UserInputText.Text.Length > 0)
            {
                UserInputText.Text = UserInputText.Text.Remove(UserInputText.Text.Length - 1, 1);
                if (UserInputText.Text.Length == 0)
                {
                    UserInputText.Text = "0";
                }
            }

        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            UserInputText.Clear();
            UserInputText.Text = "0";
            result = 0;
        }

        private void buttonLog_Click(object sender, EventArgs e)
        {
            double log = Math.Log10(double.Parse(UserInputText.Text));
            label.Text = "log(" + UserInputText.Text + ")";
            UserInputText.Text = log.ToString();
        }

        private void buttonPower2_Click(object sender, EventArgs e)
        {
            double power = Math.Pow(double.Parse(UserInputText.Text), 2);
            label.Text = UserInputText.Text + "^2";
            UserInputText.Text = power.ToString();
        }

        private void buttonSquarex_Click(object sender, EventArgs e)
        {
            double square = Math.Sqrt(double.Parse(UserInputText.Text));
            label.Text = "√" + UserInputText.Text;
            UserInputText.Text = square.ToString();
        }

        private void buttonPowerx_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (result != 0)
            {
                buttonPowerx.PerformClick();
                operationPerformed = button.Text;
                label.Text = UserInputText.Text + "^2";
                isOperationPerformed = true;
            }
            else
            {
                operationPerformed = button.Text;
                result = double.Parse(UserInputText.Text);
                label.Text = UserInputText.Text + "^2";
                isOperationPerformed = true;
            }
        }

        private void buttonSin_Click(object sender, EventArgs e)
        {
            double radian = double.Parse(UserInputText.Text) * (Math.PI / 180);
            double sin = Math.Sin(radian);
            label.Text = "sin(" + UserInputText.Text + ")";
            UserInputText.Text = sin.ToString();
        }

        private void buttonCos_Click(object sender, EventArgs e)
        {
            double radian = double.Parse(UserInputText.Text) * (Math.PI / 180);
            double cos = Math.Cos(radian);
            label.Text = "cos(" + UserInputText.Text + ")";
            UserInputText.Text = cos.ToString();
        }

        private void buttonTg_Click(object sender, EventArgs e)
        {
            double radian = double.Parse(UserInputText.Text) * (Math.PI / 180);
            double tg = Math.Tan(radian);
            label.Text = "tg(" + UserInputText.Text + ")";
            UserInputText.Text = tg.ToString();
        }

        private void buttonCtg_Click(object sender, EventArgs e)
        {
            double radian = double.Parse(UserInputText.Text) * (Math.PI / 180);
            double tg = 1/Math.Tan(radian);
            label.Text = "tg(" + UserInputText.Text + ")";
            UserInputText.Text = tg.ToString();
        }

        private void buttonPI_Click(object sender, EventArgs e)
        {
            UserInputText.Text = (Math.PI).ToString();
        }

        private void buttonZusEmployer_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
        private void buttonZusEmployee_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.Show();
        }
        private void buttonPlusMinus_Click(object sender, EventArgs e)
        {
            double operatorType = double.Parse(UserInputText.Text);
            UserInputText.Text = (-operatorType).ToString();
        }

        private void buttonNettoBrutto_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4();
            form4.Show();
        }
    }
}
