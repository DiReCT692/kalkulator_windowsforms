﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalk_WindowsForms
{
    public partial class Form2 : Form
    {
        double result = 0;
        public Form2()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double pension = 0.0976;
            double disabilityPension = 0.065;
            double accident = 0.0167;
            double FP = 0.0245;
            double FGSP = 0.001;
            double salary = double.Parse(UserInputText.Text);
            double result = (salary * pension) + (salary * disabilityPension) + (salary * accident) + (salary * FP) + (salary * FGSP);
            labelContribution.Text = ("Salary:\n" + salary + "\n\nPension:\n" + salary * pension + "\n\nDisability Pension:\n" + salary * disabilityPension +
            "\n\nAccident:\n" + salary * accident + "\n\nFP:\n" + salary * FP + "\n\nFGŚP:\n" + salary * FGSP + "\n\nSum of Contributions:\n" + result + "\n\nNet salary:\n" + (salary - result)).ToString();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (UserInputText.Text == "0")
            {
                UserInputText.Clear();
            }
            Button button = (Button)sender;
            if (button.Text == ",")
            {
                if (!UserInputText.Text.Contains(","))
                    UserInputText.Text = UserInputText.Text + button.Text;
            }
            else
            {
                UserInputText.Text = UserInputText.Text + button.Text;
            }

        }

        private void buttonBackspace_Click(object sender, EventArgs e)
        {
            if (UserInputText.Text.Length > 0)
            {
                UserInputText.Text = UserInputText.Text.Remove(UserInputText.Text.Length - 1, 1);
                if (UserInputText.Text.Length == 0)
                {
                    UserInputText.Text = "0";
                }
            }

        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            UserInputText.Clear();
            UserInputText.Text = "0";
            result = 0;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
