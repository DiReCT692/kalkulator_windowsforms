﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalk_WindowsForms
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBoxType.Text == "Brutto")
            {
                if(comboBoxValue.Text == "23%")
                {
                    double vat = double.Parse(UserInputText.Text) * 0.23;
                    double amount = double.Parse(UserInputText.Text) * 0.77;
                    label1.Text = "Gross amount: " + UserInputText.Text + "\nVAT: " + vat.ToString() + "\nNet amount: " + amount.ToString();
                } else if (comboBoxValue.Text == "8%")
                {
                    double vat = double.Parse(UserInputText.Text) * 0.08;
                    double amount = double.Parse(UserInputText.Text) * 0.92;
                    label1.Text = "Gross amount: " + UserInputText.Text + "\nVAT: " + vat.ToString() + "\nNet amount: " + amount.ToString();
                } else if (comboBoxValue.Text == "5%")
                {
                    double vat = double.Parse(UserInputText.Text) * 0.05;
                    double amount = double.Parse(UserInputText.Text) * 0.95;
                    label1.Text = "Gross amount: " + UserInputText.Text + "\nVAT: " + vat.ToString() + "\nNet amount: " + amount.ToString();
                }
            }
            if (comboBoxType.Text == "Netto")
            {
                if (comboBoxValue.Text == "23%")
                {
                    double vat = double.Parse(UserInputText.Text) * 0.23;
                    double amount = double.Parse(UserInputText.Text) * 1.23;
                    label1.Text = "Gross amount: " + UserInputText.Text + "\nVAT: " + vat.ToString() + "\nNet amount: " + amount.ToString();
                }
                else if (comboBoxValue.Text == "8%")
                {
                    double vat = double.Parse(UserInputText.Text) * 0.08;
                    double amount = double.Parse(UserInputText.Text) * 1.08;
                    label1.Text = "Gross amount: " + UserInputText.Text + "\nVAT: " + vat.ToString() + "\nNet amount: " + amount.ToString();
                }
                else if (comboBoxValue.Text == "5%")
                {
                    double vat = double.Parse(UserInputText.Text) * 0.05;
                    double amount = double.Parse(UserInputText.Text) * 1.05;
                    label1.Text = "Gross amount: " + UserInputText.Text + "\nVAT: " + vat.ToString() + "\nNet amount: " + amount.ToString();
                }
            }
        }
    }
}
